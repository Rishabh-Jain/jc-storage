<?php

Route::get('', function () {
    return redirect("/dashboard");
});
Route::get('dashboard', 'DashboardController@index');

Auth::routes();
Route::resource('server', 'ServerController')->middleware('auth');
Route::resource('video', 'VideoController')->middleware('auth');
