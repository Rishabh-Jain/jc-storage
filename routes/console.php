<?php

Artisan::command("install {--S|sampledata}", function () {
    $this->info('Installing...');
    $this->call('migrate:fresh');
    $this->call('install:addAdminRoles');
    $this->call('install:addAdmin');
    if ($this->option('sampledata')) {
		$this->call('install:sampledata');
    }
    $this->info("Software installed successfully.");
});
