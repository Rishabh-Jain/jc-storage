<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoEncodingTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_encoding_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->unsignedInteger("width");
            $table->unsignedInteger("height");
            $table->unsignedInteger("quality");
            $table->unsignedInteger("audio_bit_rate");
            $table->unsignedInteger("frame_rate")->default(30);
            $table->string("audio_codec");
            $table->text('params')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_encoding_types');
    }
}
