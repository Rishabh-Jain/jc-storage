@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card">
                <div class="card-header">
                    Add Server
                </div>
                <div class="card-block">
                    <div class="col my-3">
                        <table class="table table-bordered table-hover table-responsive-md">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#id</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">IP Address</th>
                                    <th scope="col-sm-2">Public URL</th>
                                    <th scope="col">Server ID</th>
                                    <th scope="col">Type</th>
                                    <th scope="col-sm-2">Added By</th>
                                    <th scope="col">Added</th>
                                    <th scope="col">Status</th>
                                    <th scope="col-sm-2">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($servers as $server)
                                    <tr>
                                        <td>{{ $server->id }}</td>
                                        <td>{{ $server->name }}</td>
                                        <td>{{ $server->ip }}</td>
                                        <td>{{ $server->host }}</td>
                                        <td><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" title="Server ID" data-content="{{ $server->uid }}">Show</button></td>
                                        <td>{{ $server->type }}</td>
                                        <td>{{ $server->added }}</td>
                                        <td>{{ $server->created_at }}</td>
                                        <td class="text-center">
                                            <span class="p-1 rounded bg-{{ $server->active ? 'success' : 'danger' }}">
                                                {{ $server->active ? 'Active' : 'Disabled' }}
                                            </span>
                                        </td>
                                        <td>
                                            <a class="btn-link">Edit</a>
                                            <a class="btn-link">Edit</a>
                                            <div class="w-100"></div>
                                            <a class="btn-link">Edit</a>
                                            <a class="btn-link">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
