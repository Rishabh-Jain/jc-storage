@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card">
                <div class="card-header">
                    Add Server
                </div>
                <div class="card-block">
                    <div class="col my-3">
                        <table class="table table-bordered table-hover table-responsive-md">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#id</th>
                                    <th scope="col-sm-2">Title</th>
                                    <th scope="col">Slug</th>
                                    <th scope="col-sm-2">Public URL</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Is Raw</th>
                                    <th scope="col-sm-2">Views</th>
                                    <th scope="col">Added</th>
                                    <th scope="col">Server</th>
                                    <th scope="col-sm-2">Encoding Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($videos as $video)
                                    <tr>
                                        <td>{{ $video->id }}</td>
                                        <td>{{ $video->title }}</td>
                                        <td><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" title="Server Slug" data-content="{{ $video->slug }}">Show</button></td>
                                        <td><button type="button" class="btn btn-sm btn-danger" data-toggle="popover" title="Public URL" data-content="{{ $video->link }}">Show</button></td>
                                        <td class="text-center">
                                            <span class="p-1 rounded bg-{{ $video->status ? 'success' : 'danger' }}">
                                                {{ $video->status ? 'Active' : 'Disabled' }}
                                            </span>
                                        </td>
                                        <td class="text-center">
                                            <span class="p-1 rounded bg-{{ !$video->raw ? 'success' : 'danger' }}">
                                                {{ $video->raw ? 'Raw' : 'Encoded' }}
                                            </span>
                                        </td>
                                        <td>{{ $video->views }}</td>
                                        <td>{{ $video->created_at }}</td>
                                        <td>{{ $video->server->name }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
