@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-sm-12 my-3">
            <div class="card bg-dark text-light text-bold">
                <div class="card-header">
                    Add Server
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 col-md-9">
                            @if(!empty($created))
                                <div class="alert alert-success">The video has been created.</div>
                            @endif
                            <form class="m-3" method="post" action="{{ action("VideoController@store") }}">
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>Name: </strong>
                                    </label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="title" placeholder="Video Title" value="{{ old('title') }}"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>Upload: </strong>
                                    </label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" name="vid" placeholder="Upload a file." value="{{ old('vid') }}"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2">
                                        <strong>Encoding Type: </strong>
                                    </label>
                                    <div class="input-group col-sm-4">
                                        <select name="etype" class="form-control custom-select">
                                            <option value="y" selected>Encode</option>
                                            <option value="n">No Encoding</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 ml-auto">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-primary btn-block">Add Server</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @if($errors->any())
                            <div class="col-md-3 align-self-md-start">
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
