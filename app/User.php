<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Loggable;

class User extends Authenticatable
{
    use Notifiable, HasRoles, Loggable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function logs()
    {
        return $this->hasMany("App\UserLog");
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function log($type, $attr)
    {
        if ($type === 'login') {
            $this->logs()->create($attr);
        }
    }
}
