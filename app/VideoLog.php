<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoLog extends Model
{
    protected $guarded = [];

    public function video()
    {
        return $this->belongsTo("App\Video");
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
