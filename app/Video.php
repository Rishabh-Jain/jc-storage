<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Loggable;

class Video extends Model
{
    use Loggable;

    protected $guarded = [];
    protected $append = ['raw'];

    public function getRawAttribute()
    {
        return $this->is_raw;
    }

    public function server()
    {
        return $this->belongsTo("App\Server");
    }

    public function logs()
    {
        return $this->hasMany("App\VideoLog");
    }

    public function encoding()
    {
        return $this->belongsTo("App\VideoEncodingType");
    }
}
