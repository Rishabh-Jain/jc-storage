<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Server;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servers = Server::all();
        return view("server.list")->with('servers', $servers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("server.add");
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'name'      => 'required|min:5',
            'uid'       => 'required|size:64|unique:servers',
            'secret'    => 'required|size:64|unique:servers',
            'host'      => 'required|active_url|unique:servers',
            'ip'        => 'required|ip|unique:servers',
            'storage'   => 'required|integer',
            'bandwidth' => 'required|integer',
            'type'      => 'required|in:s,c,sc',
        ]);

        $server = Server::create($request->only('name', 'uid', 'secret', 'host', 'ip', 'storage', 'bandwidth', 'type'));
        return view("server.add", ['created' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
