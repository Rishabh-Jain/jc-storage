<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $guarded = [];

    public function setUidAttribute($id)
    {
        $this->attributes['uid'] = encrypt($id);
    }

    public function setSecretAttribute($secret)
    {
        $this->attributes['secret'] = encrypt($secret);
    }

    public function getUidAttribute($id)
    {
        try {
            return decrypt($id);
        } catch (\Exception $e) {
            return 'Error';
        }
    }

    public function getSecretAttribute($secret)
    {
        try {
            return decrypt($secret);
        } catch (\Exception $e) {
            return 'Error';
        }
    }

    public function getTypeAttribute($type)
    {
        if ($type === 's') {
            return 'Storage';
        } elseif ($type === 'c') {
            return "Client";
        } else {
            return 'NONE';
        }
    }
}
