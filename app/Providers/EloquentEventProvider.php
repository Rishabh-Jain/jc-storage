<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Video;
use App\Observers\VideoObserver;

class EloquentEventProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Video::observe(VideoObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
