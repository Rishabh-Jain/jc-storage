<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class AddAdmin extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'install:addAdmin {--R|roles=admin}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Complete admin related install tasks.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {

		$system_user           = new User;
		$system_user->name     = 'system';
		$system_user->username = 'system';
		$system_user->email    = 'abuse@localhost';
		$system_user->password = str_random(12);
		$system_user->saveOrFail();

		$this->info( "Creating User." );
		$user           = new User;
		$user->name     = 'admin';
		$user->username = 'admin';
		$user->email    = 'abuse@jnexsoft.com';
		$user->password = 'appadmin';
		$user->saveOrFail();

		$user->assignRole( 'Admin' );

		$this->info( "Admin Created." );

	}
}
