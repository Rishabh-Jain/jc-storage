<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Server;
use App\VideoEncodingtype;
use App\Video;

class SampleData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:sampledata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install sample data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createServer();
        $this->createVideoEncodings();
        $this->createVideos();
    }

    public function createVideoEncodings()
    {
        $this->info("Adding video encodings.");
        $encs = [
            ['name' => '360_profile', 'width' => '480', 'height' => '360', 'quality' => '25', 'audio_bit_rate' => '64', 'audio_codec' => 'av_aac'],
            ['name' => '480_profile', 'width' => '720', 'height' => '480', 'quality' => '23', 'audio_bit_rate' => '96', 'audio_codec' => 'av_aac'],
            ['name' => '720_profile', 'width' => '1280', 'height' => '720', 'quality' => '22', 'audio_bit_rate' => '128', 'audio_codec' => 'av_aac'],
            ['name' => '1080_profile', 'width' => '1920', 'height' => '1080', 'quality' => '20', 'audio_bit_rate' => '192', 'audio_codec' => 'av_aac']
        ];

        foreach ($encs as $enc) {
            VideoEncodingtype::create($enc);
        }
        $this->info("Added video encodings.");
    }

    public function createVideos()
    {
        $this->info("Adding video.");
        Video::create([
            'title' => 'Video 1',
            'link' => 'video-link-1',
        ]);

        Video::create([
            'title' => 'Video 2',
            'link' => 'video-link-2'
        ]);
        $this->info("Added video.");
    }

    public function createServer()
    {
        $this->info("Adding server.");
        Server::create([
            'name' => 'Server 1',
            'host' => 'http://jnexsoft.com',
            'ip' => '127.0.0.1',
            'uid' => 'MbzX6dl9Zcq88GyMZQlHml6syKpBcVqX71WS1xzV4pL9a990IDGICZjL7Rgqos6h',
            'secret' => 'MbzX6dl9Zcq88GyMZQlHml6syKpBcVqX71WS1xzV4pL9a990IDGICZjL7Rgqos6h',
            'type' => 's',
            'storage' => 15000,
            'bandwidth' => 1500000,
            'active' => true,
        ]);
        $this->info("Added server.");
    }
}
