<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;

class AddAdminRoles extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'install:addAdminRoles';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Add admin roles.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->info( 'Calling Admin Roles' );
		$roles = [
			'admin',
			'operator'
		];

		foreach ( $roles as $role ) {
			Role::create( [ 'name' => $role ] );
		}

		$this->info( "Roles added." );
	}
}
