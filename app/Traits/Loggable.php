<?php
namespace App\Traits;

trait Loggable
{
    public function log($attr, $type = false)
    {
        $type = $type ? $type.'Logs' : 'logs';
        if (is_callable([$this, $type])) {
            $this->$type()->create($attr);
        } else {
            $this->logs()->create($attr);
        }
    }
}
