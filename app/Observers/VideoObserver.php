<?php
namespace App\Observers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

use App\Video;

class VideoObserver
{
    public function creating(Video $video)
    {
        $video->slug = str_slug($video->title);
        $video->hash = md5($video->slug);
        $video->link = $video->slug;
        $video->server_id = $video->server_id ?? 1;
        return $video;
    }

    public function created(Video $video)
    {
        $video->logs([
            'type' => 'created',
            'user_id' => Auth::id(),
            'log' => 'Video Created.',
            'ip' => Request::ip(),
        ]);
        return $video;
    }
}
